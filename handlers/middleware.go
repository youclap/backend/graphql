package handlers

import (
	"context"
	"net/http"
)

var headerValues = "HeaderValues"
var authUserCtxKey = "X-Auth-User"
var profileIDCtxKey = "X-Profile-ID"
var userIDCtxKey = "X-User-ID"

type Values struct {
	m map[string]string
}

func (v Values) Get(key string) string {
	return v.m[key]
}

func Middleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			authUser := r.Header.Get(authUserCtxKey)
			profileID := r.Header.Get(profileIDCtxKey)
			userID := r.Header.Get(userIDCtxKey)

			v := Values{map[string]string{
				authUserCtxKey:  authUser,
				profileIDCtxKey: profileID,
				userIDCtxKey:    userID,
			}}

			ctx := context.WithValue(r.Context(), headerValues, v)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

func GetAuthUser(ctx context.Context) string {
	return ctx.Value(headerValues).(Values).Get(authUserCtxKey)
}

func GetProfileID(ctx context.Context) string {
	return ctx.Value(headerValues).(Values).Get(profileIDCtxKey)
}

func GetUserID(ctx context.Context) string {
	return ctx.Value(headerValues).(Values).Get(userIDCtxKey)
}
