module gitlab.com/youclap/backend/graphql

go 1.12

require (
	cloud.google.com/go v0.46.3
	cloud.google.com/go/storage v1.1.0
	github.com/99designs/gqlgen v0.9.1
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.6.0
	github.com/vektah/gqlparser v1.1.2
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	google.golang.org/api v0.9.0
)
