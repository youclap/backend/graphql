package models

type FeedItems struct {
	Items     []FeedItem `json:"items"`
	NextToken string     `json:"nextToken"`
}

type FeedItem struct {
	FirebaseID string   `json:"firebaseID"`
	Type       FeedType `json:"type"`
}

type IsFollowing struct {
	IsFollowing bool `json:"isFollowing"`
}

type SearchResult struct {
	ProfileIDs []int `json:"profileIDs"`
}

type CountObject struct {
	Count int `json:"count"`
}

type ProfileUpdate struct {
	Name       string     `json:"name"`
	AvatarType AvatarType `json:"avatar"`
}
