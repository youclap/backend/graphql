package services

import (
	"context"
	"fmt"
	"io"
	"log"

	"cloud.google.com/go/storage"
	"github.com/google/uuid"
)

// StorageBucket : struct with access to our GC bucket storage
type StorageBucket struct {
	Client     *storage.Client
	BucketName string
	Bucket     *storage.BucketHandle
	Ctx        context.Context
	Logger     *log.Logger
}

// StorageService : handle upload of different contents
type StorageService struct {
	Ctx           context.Context
	Writer        io.Writer
	StorageBucket *StorageBucket
	Logger        *log.Logger
}

const (
	CHALLENGE_IMAGE_PATH = "challenge/%v"
	POST_IMAGE_PATH      = "challenge/%v/post/%v"
	USER_IMAGE_PATH      = "user/%v/%v"
	USER_PATH            = "user/%v"
	MEDIA_NAME           = "media"
)

// Upload : Upload file to bucket
func (s *StorageBucket) Upload(r io.Reader, path string) (*storage.ObjectHandle, *storage.ObjectAttrs, error) {
	s.Logger.Printf("upload file to bucket with path %s", path)

	obj := s.Bucket.Object(path)
	w := obj.NewWriter(s.Ctx)

	if _, err := io.Copy(w, r); err != nil {
		s.Logger.Printf("error writing file to path %s", path)
		return nil, nil, err
	}
	if err := w.Close(); err != nil {
		s.Logger.Printf("error closing bucket writer with error %v", err)
		return nil, nil, err
	}

	attrs, err := obj.Attrs(s.Ctx)
	if err != nil {
		s.Logger.Printf("error on object metadata with error %v", err)
		return nil, nil, err
	}

	return obj, attrs, nil
}

// HandleUserPath : Create user path for save user file on bucket
func (s *StorageBucket) HandleUserPath(userID string) string {
	s.Logger.Printf("Build path for user %s path", userID)

	location := fmt.Sprintf(USER_PATH, userID)

	s.Logger.Printf("Path location for user %s: %s", userID, location)

	return location
}

// UploadUser : Upload user file to bucket with user path
func (s *StorageBucket) UploadUser(file io.Reader, userID string) (*storage.ObjectHandle, *storage.ObjectAttrs, error) {
	s.Logger.Printf("Upload user %s", userID)

	randomUUID, err := uuid.NewRandom()
	if err != nil {
		s.Logger.Printf("Error generating UUID with error %v", err)
		return nil, nil, err
	}

	path := fmt.Sprintf("%s/%s", s.HandleUserPath(userID), randomUUID)

	s.Logger.Printf("upload user file to path %s", path)

	return s.Upload(file, path)
}

// HandleChallengePath : Create challenge path to save
func (s *StorageBucket) HandleChallengePath(challengeID string) string {
	s.Logger.Printf("Build path for challenge %s path", challengeID)

	location := fmt.Sprintf(CHALLENGE_IMAGE_PATH, challengeID)

    s.Logger.Printf("Path location for challenge %s: %s", challengeID, location)

    return location
}

// HandlePostPath : Create post path for save post file on bucket
func (s *StorageBucket) HandlePostPath(postID string, challengeID string) string {
	s.Logger.Printf("Build path for post %s", postID)

	location := fmt.Sprintf(POST_IMAGE_PATH, challengeID, postID)

	s.Logger.Printf("Path location fr post %s: %s", postID, location)

	return location
}

// UploadChallenge : Upload challenge file to bucket with challenge path
func (s *StorageBucket) UploadChallenge(file io.Reader, challengeID string) (*storage.ObjectHandle, *storage.ObjectAttrs, error) {
	s.Logger.Printf("Upload challenge %s", challengeID)

	path := fmt.Sprintf("%s/%s", s.HandleChallengePath(challengeID), MEDIA_NAME)

	s.Logger.Printf("upload challenge file to path %s", path)

    return s.Upload(file, path)
}

// UploadPost : Upload post file to bucket with post path
func (s *StorageBucket) UploadPost(file io.Reader, postID string, challengeID string) (*storage.ObjectHandle, *storage.ObjectAttrs, error) {
	s.Logger.Printf("Upload post %s ", postID)

	path := fmt.Sprintf("%s/%s", s.HandlePostPath(postID, challengeID), MEDIA_NAME)

	s.Logger.Printf("upload post file to path %s", path)

	return s.Upload(file, path)
}
