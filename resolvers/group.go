package resolvers

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/youclap/backend/graphql/models"
)

func (r *mutationResolver) CreateGroup(ctx context.Context, input models.GroupInput) (*models.Group, error) {
	r.Logger.Printf("Create group with input %v", input)

	b, err := json.Marshal(input)
	if err != nil {
		return nil, fmt.Errorf("error on marshal group input %v with error %v", input, err)
	}

	url := fmt.Sprintf("%s%s", r.Viveiro, groupURL)
	group := &models.Group{}

	if err := postJSON(r.Logger, url, headersFromContext(ctx), b, group); err != nil {
		return nil, err
	}

	return group, nil
}

func (r *mutationResolver) DeleteGroup(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Delete group id %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Viveiro, groupURL, id)

	return deleteRequest(r.Logger, url, headersFromContext(ctx))
}

func (r *mutationResolver) AddMember(ctx context.Context, groupID int, profileID int) (*models.GroupProfile, error) {
	r.Logger.Printf("Add profile id %d to group id %d", profileID, groupID)

	url := fmt.Sprintf("%s%s/%d/member/%d", r.Viveiro, groupURL, groupID, profileID)

	groupProfile := &models.GroupProfile{}
	if err := postJSON(r.Logger, url, headersFromContext(ctx), nil, groupProfile); err != nil {
		return nil, err
	}

	// TODO - role comes empty, doens't marshal roleID field
	return groupProfile, nil
}

func (r *mutationResolver) DeleteMember(ctx context.Context, groupID int, profileID int) (bool, error) {
	r.Logger.Printf("Delete member profile %d from group %d", profileID, groupID)

	url := fmt.Sprintf("%s%s/%d/member/%d", r.Viveiro, groupURL, groupID, profileID)

	return deleteRequest(r.Logger, url, headersFromContext(ctx))
}

// Group : Get group by ID
func (r *queryResolver) Group(ctx context.Context, id int) (*models.Group, error) {
	r.Logger.Printf("Get group by ID: %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Viveiro, groupURL, id)
	group := &models.Group{}

	if err := getJSON(r.Logger, url, headersFromContext(ctx), group); err != nil {
		return nil, err
	}

	return group, nil
}

// Creator : Get the group creator Profile
func (r *groupResolver) Creator(ctx context.Context, obj *models.Group) (*models.Profile, error) {
	return r.ProfileByID(ctx, obj.CreatedByProfileID)
}

// Members : Get groups members Profiles
func (r *groupResolver) Members(ctx context.Context, obj *models.Group) ([]*models.Profile, error) {
	r.Logger.Printf("Get members from group ID: %d", obj.ID)

	url := fmt.Sprintf("%s%s/%d/members", r.Viveiro, groupURL, obj.ID)
	profileIDs := make([]int, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &profileIDs); err != nil {
		return nil, err
	}

	return profilesByIDs(ctx, r.Marnoto, r.Logger, profileIDs)
}

// Groups : get groups for profile by id
func (r *queryResolver) Groups(ctx context.Context, profileID int) ([]*models.Group, error) {
	r.Logger.Printf("Get groups for profile id %d", profileID)

	url := fmt.Sprintf("%s%s?profileID=%d", r.Viveiro, groupsURL, profileID)
	groups := make([]*models.Group, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &groups); err != nil {
		return nil, err
	}

	return groups, nil
}

// Groups : get groups for a specific profile using profile resolver
func (r *profileResolver) Groups(ctx context.Context, obj *models.Profile) ([]*models.Group, error) {
	return r.Query().Groups(ctx, obj.ID)
}
