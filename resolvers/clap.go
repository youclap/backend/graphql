package resolvers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/youclap/backend/graphql/models"
)

func (r *mutationResolver) CreateClap(ctx context.Context, input models.ClapInput) (*models.Clap, error) {
	r.Logger.Printf("Create clap with input %v", input)

	clap := &models.Clap{}

	url := fmt.Sprintf("%s%s", r.Dopamina, clapURL)

	b, err := json.Marshal(input)
	if err != nil {
		r.Logger.Printf("Error on marshal clap input with error %v", err)
		return nil, err
	}

	if err := postJSON(r.Logger, url, headersFromContext(ctx), b, clap); err != nil {
		return clap, err
	}

	return clap, nil
}

func (r *mutationResolver) DeleteClap(ctx context.Context, postID int) (*models.Clap, error) {
	r.Logger.Printf("Delete clap for postID: %d", postID)

	url := fmt.Sprintf("%s%s?postID=%d", r.Dopamina, clapURL, postID)
	headers := headersFromContext(ctx)
	deleted, err := deleteRequest(r.Logger, url, headers)

	// TODO - update this hammer
	profileID, _ := strconv.Atoi(headers.profileID)
	if deleted {
		return &models.Clap{
			PostID:      postID,
			ProfileID:   profileID,
			Type:        models.ClapTypeNone,
			CreatedDate: time.Now(),
		}, nil
	}

	return nil, err
}

// ClapsCount : get number of claps for a post id
func (r *postResolver) ClapsCount(ctx context.Context, obj *models.Post) (int, error) {
	r.Logger.Printf("Get claps count for post %d", obj.ID)

	url := fmt.Sprintf("%s%s/count?postID=%d", r.Dopamina, clapsURL, obj.ID)

	counter := &models.CountObject{}
	if err := getJSON(r.Logger, url, headersFromContext(ctx), counter); err != nil {
		return 0, err
	}

	r.Logger.Printf("Post ID: %d has %d claps", obj.ID, counter.Count)

	return counter.Count, nil
}

// Clapped : check if a Profile has clapped a post
func (r *postResolver) Clapped(ctx context.Context, obj *models.Post) (*models.ClapType, error) {

	url := fmt.Sprintf("%s%s?postID=%d", r.Dopamina, clapURL, obj.ID)

	//TODO change this
	client := &http.Client{}
	req, _ := http.NewRequest(http.MethodGet, url, nil)

	req.Header.Add("X-Profile-ID", addHammerProfileID(ctx).profileID)

	resp, err := client.Do(req)

	if resp != nil {
		code := resp.StatusCode
		defer resp.Body.Close()

		r.Logger.Printf("url: %s, response status code: %d", url, code)

		clap := &models.Clap{}

		switch code {
		case http.StatusOK:
			unmarshalJSON(r.Logger, resp, clap)
			return &clap.Type, nil

		case http.StatusNotFound:
			clap.Type = models.ClapTypeNone
			return &clap.Type, nil

		default:
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("error on request clap for postID %d with error %v", obj.ID, err)
	}
}

// Post : Get post for a clap
func (r *clapResolver) Post(ctx context.Context, obj *models.Clap) (*models.Post, error) {
	return r.Query().Post(ctx, obj.PostID)
}
