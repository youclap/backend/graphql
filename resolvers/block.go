package resolvers

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/youclap/backend/graphql/models"
)

// Block : when a profile blocks another profile
func (r *mutationResolver) Block(ctx context.Context, profileID int, blockProfileID int) (bool, error) {
	r.Logger.Printf("Profile %d blocks profile %d", profileID, blockProfileID)

	url := fmt.Sprintf("%s%s/%d%s/%d", r.Marnoto, profileURL, profileID, blockURL, blockProfileID)

	if _, err := validateRequest(r.Logger, http.MethodPost, nil, url, headersFromContext(ctx)); err != nil {
		r.Logger.Printf("error on profile %d blocking profile %d with error %v", profileID, blockProfileID, err)
		return false, err
	}

	return true, nil
}

// Block : when a profile unblocks another profile
func (r *mutationResolver) Unblock(ctx context.Context, profileID int, blockedProfileID int) (bool, error) {
	r.Logger.Printf("Profile %d unblocks profile %d", profileID, blockedProfileID)

	url := fmt.Sprintf("%s%s/%d%s/%d", r.Marnoto, profileURL, profileID, blockURL, blockedProfileID)

	if _, err := validateRequest(r.Logger, http.MethodDelete, nil, url, headersFromContext(ctx)); err != nil {
		r.Logger.Printf("error on profile %d blocking profile %d with error %v", profileID, blockedProfileID, err)
		return false, err
	}

	return true, nil
}

// BlockedProfiles : Get blocked profiles from a profile
func (r *profileResolver) BlockedProfiles(ctx context.Context, obj *models.Profile) ([]*models.Profile, error) {
	r.Logger.Printf("Get profiles blocked by profile %d", obj.ID)

	url := fmt.Sprintf("%s%s/%d%s", r.Marnoto, profileURL, obj.ID, blockedURL)
	blockedProfiles := make([]*models.Profile, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &blockedProfiles); err != nil {
		r.Logger.Printf("error getting blocked profiles for profile %d with error %v", obj.ID, err)
		return nil, err
	}

	r.Logger.Printf("profile id %d has blocked %d profiles", obj.ID, len(blockedProfiles))
	return blockedProfiles, nil
}
