package resolvers

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/youclap/backend/graphql/models"
)

func (r *mutationResolver) CreateUser(ctx context.Context, input models.UserInput) (*models.User, error) {
	r.Logger.Printf("Create user with user input %v\n", input)

	url := fmt.Sprintf("%s%s", r.Marnoto, userURL)

	b, _ := json.Marshal(input)
	user := &models.User{}
	if err := postJSON(r.Logger, url, headersFromContext(ctx), b, user); err != nil {
		return nil, err
	}

	r.Logger.Printf("Created user %d", user.ID)

	return user, nil
}

func (r *mutationResolver) UpdateUser(ctx context.Context, changes map[string]interface{}) (*models.User, error) {
	headers := headersFromContext(ctx)

	r.Logger.Printf("Update user %s with these changes %v", headers.userID, changes)

	url := fmt.Sprintf("%s%s", r.Marnoto, userURL)

	updatedUser := &models.User{}
	if err := patchJSON(r.Logger, url, headers, changes, updatedUser); err != nil {
		return nil, err
	}

	return updatedUser, nil
}
