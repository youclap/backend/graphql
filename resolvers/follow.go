package resolvers

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/youclap/backend/graphql/models"
)

func (r *mutationResolver) Follow(ctx context.Context, followerProfileID int, followingProfileID int) (*models.Profile, error) {
	r.Logger.Printf("Profile id %d starts following profile %d", followerProfileID, followingProfileID)

	url := fmt.Sprintf("%s%s/%d%s/%d", r.Marnoto, profileURL, followerProfileID, followURL, followingProfileID)

	if _, err := validateRequest(r.Logger, http.MethodPost, nil, url, headersFromContext(ctx)); err != nil {
		r.Logger.Printf("error on follow followerProfileID %d with followingProfileID %d with error %v", followerProfileID, followingProfileID, err)
		return nil, err
	}

	return r.ProfileByID(ctx, followingProfileID)
}

func (r *mutationResolver) Unfollow(ctx context.Context, followerProfileID int, followingProfileID int) (*models.Profile, error) {
	r.Logger.Printf("Profile id%d unfollows profile %d", followerProfileID, followingProfileID)

	url := fmt.Sprintf("%s%s/%d%s/%d", r.Marnoto, profileURL, followerProfileID, followURL, followingProfileID)

	if _, err := validateRequest(r.Logger, http.MethodDelete, nil, url, headersFromContext(ctx)); err != nil {
		r.Logger.Printf("error on unfollow followerProfileID %d with followingProfileID %d with error %v", followerProfileID, followingProfileID, err)
		return nil, err
	}

	return r.ProfileByID(ctx, followingProfileID)
}

// TODO - needs profileID header to work. Marnoto is expeting profileID
// Followers : get list of profiles that follow a specific profile
func (r *profileResolver) Followers(ctx context.Context, obj *models.Profile) ([]*models.Profile, error) {

	url := fmt.Sprintf("%s%s/%d%s", r.Marnoto, profileURL, obj.ID, followersURL)
	followers := make([]*models.Profile, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &followers); err != nil {
		return nil, err
	}

	return followers, nil
}

// FollowersCount : get number of followers for a profile
func (r *profileResolver) FollowersCount(ctx context.Context, obj *models.Profile) (int, error) {

	url := fmt.Sprintf("%s%s/%d%s/count", r.Marnoto, profileURL, obj.ID, followersURL)
	counter := &models.CountObject{}

	if err := getJSON(r.Logger, url, headersFromContext(ctx), counter); err != nil {
		return 0, err
	}

	return counter.Count, nil
}

// TODO - needs profileID header to work. Marnoto is expeting profileID
// Following : get list of profiles followed by a specific profile
func (r *profileResolver) Following(ctx context.Context, obj *models.Profile) ([]*models.Profile, error) {

	url := fmt.Sprintf("%s%s/%d%s", r.Marnoto, profileURL, obj.ID, followingURL)
	following := make([]*models.Profile, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &following); err != nil {
		return nil, err
	}

	return following, nil
}

// FollowingCount : get number of profiles followed by a specific profile
func (r *profileResolver) FollowingCount(ctx context.Context, obj *models.Profile) (int, error) {

	url := fmt.Sprintf("%s%s/%d%s/count", r.Marnoto, profileURL, obj.ID, followingURL)
	counter := &models.CountObject{}

	if err := getJSON(r.Logger, url, headersFromContext(ctx), counter); err != nil {
		return 0, err
	}

	return counter.Count, nil
}

// IsFollowing : check if the current/session profile is following an specific profile
func (r *profileResolver) IsFollowing(ctx context.Context, obj *models.Profile) (bool, error) {
	headers := headersFromContext(ctx)
	url := fmt.Sprintf("%s%s/%s%s/%d", r.Marnoto, profileURL, headers.profileID, followingURL, obj.ID)
	state := &models.IsFollowing{}

	if err := getJSON(r.Logger, url, headers, state); err != nil {
		return false, nil
	}
	return state.IsFollowing, nil
}
