package resolvers

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/99designs/gqlgen/graphql"
	"gitlab.com/youclap/backend/graphql/models"
)

func (r *mutationResolver) CreatePublicChallenge(ctx context.Context, input models.PublicChallengeInput, file graphql.Upload) (*models.Challenge, error) {
	r.Logger.Printf("Create public challenge with input %v", input)
	return r.CreateChallenge(ctx, input, file)
}

func (r *mutationResolver) CreatePrivateProfilesChallenge(ctx context.Context, input models.PrivateProfilesChallengeInput, file graphql.Upload) (*models.Challenge, error) {
	r.Logger.Printf("Create private profile challenge with input %v", input)
	return r.CreateChallenge(ctx, input, file)
}

func (r *mutationResolver) CreatePrivateGroupChallenge(ctx context.Context, input models.PrivateGroupChallengeInput, file graphql.Upload) (*models.Challenge, error) {
	r.Logger.Printf("Create private group challenge with input %v", input)
	return r.CreateChallenge(ctx, input, file)
}

func (r *mutationResolver) CreateChallenge(ctx context.Context, input interface{}, file graphql.Upload) (*models.Challenge, error) {
	url := fmt.Sprintf("%s%s", r.Dopamina, challengeURL)

	b, err := json.Marshal(input)
	if err != nil {
		r.Logger.Printf("error on marshal challenge input %v with error %v", input, err)
	}

	var jsonObject map[string]interface{}
	if err := postJSON(r.Logger, url, headersFromContext(ctx), b, &jsonObject); err != nil {
		return nil, err
	}

	challenge, err := jsonToChallenge(r.Logger, jsonObject)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshal json %v to challenge struct with error %v", jsonObject, err)
	}

	r.Logger.Printf("Prepare to upload challenge %s file to bucket", challenge.FirebaseID)

	_, attrs, err := r.StorageBucket.UploadChallenge(file.File, challenge.FirebaseID)
	if err != nil {
		r.Logger.Printf("Error on uploading challenge %s file with error %v", challenge.FirebaseID, err)
		return nil, fmt.Errorf("Error uploading file for challenge firebase id %s", challenge.FirebaseID)
	}

	r.Logger.Printf("File %s with Content-Type %s uploaded at time %v with success", attrs.Name, attrs.ContentType, attrs.Created)

	return challenge, nil
}

func (r *mutationResolver) DeleteChallenge(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Delete challenge id %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Dopamina, challengeURL, id)

	return deleteRequest(r.Logger, url, headersFromContext(ctx))
}

// Profile : Get profile challenge creator
func (r *challengeResolver) Profile(ctx context.Context, obj *models.Challenge) (*models.Profile, error) {
	return r.ProfileByID(ctx, obj.ProfileID)
}

// PostsCount : Get number of posts for a challenge
func (r *challengeResolver) PostsCount(ctx context.Context, obj *models.Challenge) (int, error) {

	url := fmt.Sprintf("%s%s/count?challengeID=%d", r.Dopamina, postsURL, obj.ID)
	counter := &models.CountObject{}

	if err := getJSON(r.Logger, url, addHammerProfileID(ctx), counter); err != nil {
		return 0, err
	}

	return counter.Count, nil
}

// ChallengeByID : get challenge by id
func (r *Resolver) ChallengeByID(ctx context.Context, id int) (*models.Challenge, error) {
	r.Logger.Printf("Get challenge by ID: %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Dopamina, challengeURL, id)

	var jsonObject map[string]interface{}
	if err := getJSON(r.Logger, url, addHammerProfileID(ctx), &jsonObject); err != nil {
		return nil, err
	}

	challenge, err := jsonToChallenge(r.Logger, jsonObject)
	if err != nil {
		return nil, fmt.Errorf("error unmarshal json to challenge struct with error %v", err)
	}

	return challenge, nil
}

// Challenge : get challenge by id
func (r *queryResolver) Challenge(ctx context.Context, id int) (*models.Challenge, error) {
	return r.ChallengeByID(ctx, id)
}

// Challenges : get challenges created by a profile
func (r *queryResolver) Challenges(ctx context.Context, profileID int) ([]*models.Challenge, error) {
	r.Logger.Printf("Get challenges from profile ID: %d", profileID)

	url := fmt.Sprintf("%s%s?profileID=%d", r.Dopamina, challengesURL, profileID)

	var jsonObject []map[string]interface{}
	if err := getJSON(r.Logger, url, addHammerProfileID(ctx), &jsonObject); err != nil {
		return nil, err
	}

	challenges := make([]*models.Challenge, 0)
	for _, data := range jsonObject {
		challenge, _ := jsonToChallenge(r.Logger, data)
		challenges = append(challenges, challenge)
	}

	r.Logger.Printf("Number of challenges from profile ID %d: %d", profileID, len(challenges))

	return challenges, nil
}

func (r *queryResolver) GroupChallenges(ctx context.Context, groupID int) ([]*models.Challenge, error) {
	r.Logger.Printf("Get challenges from group ID: %d", groupID)

	url := fmt.Sprintf("%s%s?groupID=%d", r.Dopamina, challengesURL, groupID)

	var jsonObject []map[string]interface{}
	if err := getJSON(r.Logger, url, headersFromContext(ctx), &jsonObject); err != nil {
		return nil, err
	}

	challenges := make([]*models.Challenge, 0)
	for _, data := range jsonObject {
		challenge, _ := jsonToChallenge(r.Logger, data)
		challenges = append(challenges, challenge)
	}

	r.Logger.Printf("Number of challenges from group ID %d: %d", groupID, len(challenges))
	return challenges, nil
}

func (r *groupResolver) Challenges(ctx context.Context, obj *models.Group) ([]*models.Challenge, error) {
	return r.Query().GroupChallenges(ctx, obj.ID)
}
