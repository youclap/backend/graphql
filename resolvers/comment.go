package resolvers

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/youclap/backend/graphql/models"
)

// Comment : query a comment by id
func (r *queryResolver) Comment(ctx context.Context, id int) (*models.Comment, error) {
	r.Logger.Printf("Get comment by ID: %d ", id)

	url := fmt.Sprintf("%s%s/%d", r.Dopamina, commentURL, id)
	comment := &models.Comment{}

	if err := getJSON(r.Logger, url, addHammerProfileID(ctx), comment); err != nil {
		return nil, err
	}

	return comment, nil
}

func (r *mutationResolver) CreateComment(ctx context.Context, input models.CommentInput) (*models.Comment, error) {
	r.Logger.Printf("Create comment with input %v", input)

	url := fmt.Sprintf("%s%s", r.Dopamina, commentURL)

	b, err := json.Marshal(input)
	if err != nil {
		return nil, fmt.Errorf("Error on marshal comment input with error %v", err)
	}

	comment := &models.Comment{}
	if err := postJSON(r.Logger, url, addHammerProfileID(ctx), b, comment); err != nil {
		return nil, fmt.Errorf("error creating comment with input %v with error %v", input, err)
	}

	return comment, nil
}

func (r *mutationResolver) DeleteComment(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Delete comment %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Dopamina, commentURL, id)

	return deleteRequest(r.Logger, url, addHammerProfileID(ctx))
}

// Profile : get profile from a comment
func (r *commentResolver) Profile(ctx context.Context, obj *models.Comment) (*models.Profile, error) {
	r.Logger.Printf("Get Profile id %d from comment %d resolver", obj.ProfileID, obj.ID)
	return r.ProfileByID(ctx, obj.ProfileID)
}

// Comments : get comments for a post
func (r *postResolver) Comments(ctx context.Context, obj *models.Post) ([]*models.Comment, error) {
	return r.Query().PostComments(ctx, obj.ID)
}

// Comments : get comments for a post
func (r *queryResolver) PostComments(ctx context.Context, postID int) ([]*models.Comment, error) {
	r.Logger.Printf("Get comments for post ID: %d", postID)

	url := fmt.Sprintf("%s%s?postID=%d", r.Dopamina, commentsURL, postID)
	comments := make([]*models.Comment, 0)

	if err := getJSON(r.Logger, url, addHammerProfileID(ctx), &comments); err != nil {
		return nil, err
	}

	r.Logger.Printf("Post ID: %d has %d comments", postID, len(comments))

	return comments, nil
}

// CommentsCount : get comments number for a post
func (r *postResolver) CommentsCount(ctx context.Context, obj *models.Post) (int, error) {
	r.Logger.Printf("Get comments count for post %d resolver", obj.ID)

	url := fmt.Sprintf("%s%s/count?postID=%d", r.Dopamina, commentsURL, obj.ID)

	counter := &models.CountObject{}
	if err := getJSON(r.Logger, url, headersFromContext(ctx), counter); err != nil {
		return 0, err
	}

	r.Logger.Printf("Post ID: %d has %d comments", obj.ID, counter.Count)

	return counter.Count, nil
}
