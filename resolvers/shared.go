package resolvers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func addHeadersToRequest(req *http.Request, headers RequestHeaders) {
	req.Header.Add("X-Auth-User", headers.authUser)

	if headers.profileID != "" {
		req.Header.Add("X-Profile-ID", headers.profileID)
	}
	if headers.userID != "" {
		req.Header.Add("X-User-ID", headers.userID)
	}

	req.Header.Add("Content-Type", "application/json") //needed for post requests
}

//Validate client request
func validateRequest(logger *log.Logger, httpMethod string, requestBody []byte, url string, headers RequestHeaders) (*http.Response, error) {
	client := &http.Client{}
	req, err := http.NewRequest(httpMethod, url, bytes.NewBuffer(requestBody))
	if err != nil {
		logger.Printf("error creating new request %s, with error %v", url, err.Error())
		return nil, err
	}

	addHeadersToRequest(req, headers)
	resp, err := client.Do(req)

	if resp != nil {
		code := resp.StatusCode

		logger.Printf("url: %s, response status code: %d", url, code)

		switch code {
		case http.StatusUnauthorized:
			return nil, fmt.Errorf("%d - status unauthorized", code)

		case http.StatusInternalServerError:
			return nil, fmt.Errorf("%d - status interval server error", code)

		case http.StatusOK, http.StatusCreated, http.StatusAccepted, http.StatusNonAuthoritativeInfo,
			http.StatusNoContent, http.StatusResetContent, http.StatusPartialContent, http.StatusMultiStatus,
			http.StatusAlreadyReported, http.StatusIMUsed:
			return resp, nil

		default:
			logger.Printf("unknown error - not an 2xx response status code: %d", code)
			return nil, fmt.Errorf("unknown error - status code: %d", code)
		}
	} else {
		return nil, fmt.Errorf("error on %s request %s with error %v", httpMethod, url, err)
	}
}

func unmarshalJSON(logger *log.Logger, response *http.Response, target interface{}) error {
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.Printf("error reading response body with error %v", err)
		return err
	}

	if err := json.Unmarshal(body, &target); err != nil {
		logger.Printf("error on unmarshal target with error %v", err)
		return err
	}

	return nil
}

//Get response body from a GET request and do the unmarshal to an object
func getJSON(logger *log.Logger, url string, headers RequestHeaders, target interface{}) error {
	resp, err := validateRequest(logger, http.MethodGet, nil, url, headers)
	if err != nil {
		logger.Printf("error validating request with error %v", err)
		return err
	}
	defer resp.Body.Close()

	if err := unmarshalJSON(logger, resp, target); err != nil {
		logger.Printf("error on unmarshal body to target with error %v", err)
		return err
	}

	return nil
}

//Create a post request
func postJSON(logger *log.Logger, url string, headers RequestHeaders, requestBody []byte, target interface{}) error {
	resp, err := validateRequest(logger, http.MethodPost, requestBody, url, headers)
	if err != nil {
		logger.Printf("error validating request with error %v", err)
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.Printf("error reading response body with error %v", err)
		return err
	}

	if err := json.Unmarshal(body, &target); err != nil {
		logger.Printf("error on unmarshal target with error %v", err)
		return err
	}

	return nil
}

func deleteRequest(logger *log.Logger, url string, headers RequestHeaders) (bool, error) {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		logger.Printf("error creating HTTP %s request url %s, with error %v", http.MethodDelete, url, err)
		return false, err
	}

	addHeadersToRequest(req, headers)
	res, err := client.Do(req)

	if res != nil {
		code := res.StatusCode

		logger.Printf("url: %s, response status code: %d", url, code)

		switch code {
		case http.StatusNoContent, http.StatusOK:
			return true, nil
		case http.StatusNotFound:
			return false, nil
		default:
			return false, fmt.Errorf("Unknown response status code: %d, it's expecting 204 or 404 only", code)
		}
	} else {
		return false, fmt.Errorf("error on HTTP Delete request %s with error %v", url, err)
	}
}

func patchJSON(logger *log.Logger, url string, headers RequestHeaders, changes map[string]interface{}, target interface{}) error {
	b, _ := json.Marshal(changes)
	resp, err := validateRequest(logger, http.MethodPatch, b, url, headers)
	if err != nil {
		logger.Printf("error validanting request with error %v", err)
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.Printf("error reading response body with error %v", err)
		return err
	}
	if err := json.Unmarshal(body, &target); err != nil {
		logger.Printf("error on unmarshal target with error %v", err)
		return err
	}

	return nil
}
