package resolvers

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/99designs/gqlgen/graphql"
	"gitlab.com/youclap/backend/graphql/models"
)

func (r *mutationResolver) CreateProfile(ctx context.Context, input models.ProfileInput, file *graphql.Upload) (*models.Profile, error) {
	r.Logger.Printf("Create profile with profile input %v", input)

	url := fmt.Sprintf("%s%s", r.Marnoto, profileURL)

	b, _ := json.Marshal(input)
	profile := &models.Profile{}
	if err := postJSON(r.Logger, url, headersFromContext(ctx), b, profile); err != nil {
		return nil, err
	}

	if file != nil {
		userFirebaseID := headersFromContext(ctx).authUser

		if err := r.UploadProfilePhoto(userFirebaseID, file); err != nil {
			r.Logger.Printf("Error uploading profile photo for user firebase id %s with error %v", userFirebaseID, err)
			return nil, err
		}
	}

	r.Logger.Printf("Created profile %v", profile)
	return profile, nil
}

func (r *mutationResolver) UpdateProfile(ctx context.Context, id int, changes map[string]interface{}, file *graphql.Upload) (*models.Profile, error) {
	r.Logger.Printf("Update profile %d with changes %v", id, changes)

	url := fmt.Sprintf("%s%s/%d", r.Marnoto, profileURL, id)

	profileUpdatedData := &models.ProfileUpdate{}

	if file != nil {
		userFirebaseID := headersFromContext(ctx).authUser

		if err := r.UploadProfilePhoto(userFirebaseID, file); err != nil {
			r.Logger.Printf("Error uploading updated profile photo for user firebase id %s with error %v", userFirebaseID, err)
			return nil, err
		}

		//TODO - review this later
		changes["avatar"] = models.AvatarTypeLocal
	}

	if err := patchJSON(r.Logger, url, headersFromContext(ctx), changes, profileUpdatedData); err != nil {
		return nil, err
	}

	r.Logger.Printf("Updated profile %v", profileUpdatedData)
	return r.ProfileByID(ctx, id)
}

func (r *mutationResolver) DeleteProfile(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Delete profile id %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Marnoto, profileURL, id)

	return deleteRequest(r.Logger, url, headersFromContext(ctx))
}

// SignUp : SignUp transaction - create User and Profile (all in)
func (r *mutationResolver) SignUp(ctx context.Context, input models.SignUpInput, file *graphql.Upload) (*models.Profile, error) {
	r.Logger.Printf("Sign up with input %v", input)

	url := fmt.Sprintf("%s%s", r.Marnoto, signUpURL)

	b, _ := json.Marshal(input)

	profile := &models.Profile{}
	if err := postJSON(r.Logger, url, headersFromContext(ctx), b, profile); err != nil {
		return nil, err
	}

	if file != nil {
		userFirebaseID := headersFromContext(ctx).authUser

		if err := r.UploadProfilePhoto(userFirebaseID, file); err != nil {
			r.Logger.Printf("Error uploading profile photo for user firebase id %s with error %v", userFirebaseID, err)
			return nil, err
		}
	}

	r.Logger.Printf("Sign up with success - created profile %v", profile)
	return profile, nil
}

// UploadProfilePhoto : upload user firebase ID photo to bucket
func (r *mutationResolver) UploadProfilePhoto(userFirebaseID string, file *graphql.Upload) error {
	r.Logger.Printf("Prepare to upload user %s file to bucket", userFirebaseID)

	_, attrs, err := r.StorageBucket.UploadUser(file.File, userFirebaseID)
	if err != nil {
		r.Logger.Printf("Error on uploading user %s file with error %v", userFirebaseID, err)
		return fmt.Errorf("Error on uploading user %s file with error %v", userFirebaseID, err)
	}

	r.Logger.Printf("File %s with Content-Type %s uploaded at time %v with success", attrs.Name, attrs.ContentType, attrs.Created)
	return nil
}

// Get friends for profile by ID
func (r *queryResolver) Friends(ctx context.Context, profileID int) ([]*models.Profile, error) {
	r.Logger.Printf("Get friends for profile id %d", profileID)

	url := fmt.Sprintf("%s%s/%d%s", r.Marnoto, profileURL, profileID, friendsURL)
	profiles := make([]*models.Profile, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &profiles); err != nil {
		return nil, err
	}

	return profiles, nil
}

// Get friends for profile by ID using profile resolver
func (r *profileResolver) Friends(ctx context.Context, obj *models.Profile) ([]*models.Profile, error) {
	return r.Query().Friends(ctx, obj.ID)
}
