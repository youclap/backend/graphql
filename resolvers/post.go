package resolvers

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/99designs/gqlgen/graphql"
	"gitlab.com/youclap/backend/graphql/models"
)

// CreatePost : create Image | Video Post
func (r *mutationResolver) CreatePost(ctx context.Context, input models.PostInput, file graphql.Upload) (*models.Post, error) {
	r.Logger.Printf("Create %s post with input %v", input.Type, input)
	post, err := r.Resolver.CreatePost(ctx, input)
	if err != nil {
		r.Logger.Printf("error creating post with input %v with error %v", input, err)
		return nil, err
	}

	// Get challenge - need Challenge FirebaseID to upload the file to the right path
	challenge, err := r.ChallengeByID(ctx, post.ChallengeID)
	if err != nil {
		return nil, fmt.Errorf("error getting challenge %d with error %v", post.ChallengeID, err)
	}

	r.Logger.Printf("post challenge fid %s and post fid %s", challenge.FirebaseID, post.FirebaseID)

	_, attrs, err := r.StorageBucket.UploadPost(file.File, post.FirebaseID, challenge.FirebaseID)
	if err != nil {
		r.Logger.Printf("error uploading post %s file with error %v", post.FirebaseID, err)
		return nil, fmt.Errorf("Error uploading file for challenge %s post firebase id %s", post.Challenge.FirebaseID, post.FirebaseID)
	}

	r.Logger.Printf("File %s with Content-Type %s uploaded at time %v with success", attrs.Name, attrs.ContentType, attrs.Created)

	return post, nil
}

func (r *mutationResolver) CreatePostText(ctx context.Context, input models.PostTextInput) (*models.Post, error) {
	r.Logger.Printf("Create Post Text with input %v", input)
	return r.Resolver.CreatePost(ctx, input)
}

// CreatePost : create post
func (r *Resolver) CreatePost(ctx context.Context, input interface{}) (*models.Post, error) {

	url := fmt.Sprintf("%s%s", r.Dopamina, postURL)

	b, err := json.Marshal(input)
	if err != nil {
		r.Logger.Printf("error on marshal post input %v with error %v", input, err)
	}

	var jsonObject map[string]interface{}
	if err := postJSON(r.Logger, url, headersFromContext(ctx), b, &jsonObject); err != nil {
		return nil, err
	}

	post, err := jsonToPost(r.Logger, jsonObject)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshal json %v to challenge struct with error %v", jsonObject, err)
	}

	return post, nil
}

func (r *mutationResolver) DeletePost(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Delete post id %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Dopamina, postURL, id)

	return deleteRequest(r.Logger, url, headersFromContext(ctx))
}

// Profile : Get Post creator profile
func (r *postResolver) Profile(ctx context.Context, obj *models.Post) (*models.Profile, error) {
	return r.ProfileByID(ctx, obj.ProfileID)
}

// Challenge : Get challenge from Post participation
func (r *postResolver) Challenge(ctx context.Context, obj *models.Post) (*models.Challenge, error) {
	return r.ChallengeByID(ctx, obj.ChallengeID)
}

// Post : Get Post by ID
func (r *queryResolver) Post(ctx context.Context, id int) (*models.Post, error) {
	r.Logger.Printf("Get post by ID: %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Dopamina, postURL, id)

	var jsonObject map[string]interface{}
	if err := getJSON(r.Logger, url, addHammerProfileID(ctx), &jsonObject); err != nil {
		return nil, err
	}

	return jsonToPost(r.Logger, jsonObject)
}

// Posts : Get Posts for a profile
func (r *queryResolver) Posts(ctx context.Context, profileID int) ([]*models.Post, error) {
	r.Logger.Printf("Get posts by profile ID: %d", profileID)

	url := fmt.Sprintf("%s%s?profileID=%d", r.Dopamina, postsURL, profileID)

	var jsonObject []map[string]interface{}
	if err := getJSON(r.Logger, url, headersFromContext(ctx), &jsonObject); err != nil {
		return nil, err
	}

	posts := make([]*models.Post, 0)
	for _, data := range jsonObject {
		post, _ := jsonToPost(r.Logger, data)
		posts = append(posts, post)
	}

	return posts, nil
}

// ChallengePosts : Get Posts for a challenge
func (r *queryResolver) ChallengePosts(ctx context.Context, challengeID int, sort models.PostSort) ([]*models.Post, error) {
	r.Logger.Printf("Get posts from challenge ID: %d", challengeID)

	url := fmt.Sprintf("%s%s?challengeID=%d&sort=%s", r.Dopamina, postsURL, challengeID, sort)

	var jsonObject []map[string]interface{}
	if err := getJSON(r.Logger, url, addHammerProfileID(ctx), &jsonObject); err != nil {
		return nil, err
	}

	posts := make([]*models.Post, 0)
	for _, data := range jsonObject {
		post, _ := jsonToPost(r.Logger, data)
		posts = append(posts, post)
	}

	return posts, nil
}
