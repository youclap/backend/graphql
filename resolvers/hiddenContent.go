package resolvers

import (
	"context"
	"fmt"
	"log"
	"net/http"
)

// TODO - in all resolvers it's required profileID and userID headers

// HideChallenge : hide challenge by id
func (r *mutationResolver) HideChallenge(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Hide challenge %d", id)

	url := fmt.Sprintf("%s%s/%d%s", r.Dopamina, challengeURL, id, hideURL)
	return hideContent(ctx, r.Logger, url)
}

// UnhideChallenge : restore challenge by id
func (r *mutationResolver) UnhideChallenge(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Restore challenge %d", id)

	url := fmt.Sprintf("%s%s/%d%s", r.Dopamina, challengeURL, id, hideURL)
	return restoreContent(ctx, r.Logger, url)
}

// HidePost : hide post by id
func (r *mutationResolver) HidePost(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Hide Post %d", id)

	url := fmt.Sprintf("%s%s/%d%s", r.Dopamina, postURL, id, hideURL)
	return hideContent(ctx, r.Logger, url)
}

// UnhidePost : restore post by id
func (r *mutationResolver) UnhidePost(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Restore post %d", id)

	url := fmt.Sprintf("%s%s/%d%s", r.Dopamina, postURL, id, hideURL)
	return restoreContent(ctx, r.Logger, url)
}

// HideComment : hide comment by id
func (r *mutationResolver) HideComment(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Hide comment %d", id)

	url := fmt.Sprintf("%s%s/%d%s", r.Dopamina, commentURL, id, hideURL)
	return hideContent(ctx, r.Logger, url)
}

// UnhideComment : restore comment by id
func (r *mutationResolver) UnhideComment(ctx context.Context, id int) (bool, error) {
	r.Logger.Printf("Restore comment %d", id)

	url := fmt.Sprintf("%s%s/%d%s", r.Dopamina, commentURL, id, hideURL)
	return restoreContent(ctx, r.Logger, url)
}

func hideContent(ctx context.Context, logger *log.Logger, url string) (bool, error) {
	if _, err := validateRequest(logger, http.MethodPost, nil, url, headersFromContext(ctx)); err != nil {
		logger.Printf("error on hide content from endpoint %s with error %v", url, err)
		return false, err
	}
	return true, nil
}

func restoreContent(ctx context.Context, logger *log.Logger, url string) (bool, error) {
	if _, err := validateRequest(logger, http.MethodDelete, nil, url, headersFromContext(ctx)); err != nil {
		logger.Printf("error on unhide content from endpoint %s with error %v", url, err)
		return false, err
	}
	return true, nil
}
