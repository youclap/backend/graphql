package resolvers

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/youclap/backend/graphql"
	"gitlab.com/youclap/backend/graphql/handlers"
	"gitlab.com/youclap/backend/graphql/models"
	"gitlab.com/youclap/backend/graphql/services"
)

const (
	userURL       string = "/user"
	profileURL    string = "/profile"
	profilesURL   string = "/profiles"
	searchURL     string = "/search"
	challengeURL  string = "/challenge"
	challengesURL string = "/challenges"
	exploreURL    string = "/explore"
	featuredURL          = exploreURL + "/featured"
	trendingURL          = exploreURL + "/trending"
	postURL       string = "/post"
	postsURL      string = "/posts"
	commentURL    string = "/comment"
	commentsURL   string = "/comments"
	clapURL       string = "/clap"
	clapsURL      string = "/claps"
	feedURL       string = "/feed"
	followURL     string = "/follow"
	followersURL  string = "/followers"
	followingURL  string = "/following"
	groupURL      string = "/group"
	groupsURL     string = "/groups"
	hideURL       string = "/hide"
	blockURL      string = "/block"
	blockedURL    string = "/blocked"
	signUpURL     string = "/signup"
	friendsURL    string = "/friends"
)

// RequestHeaders : object to store request headers from the client
type RequestHeaders struct {
	authUser  string
	profileID string
	userID    string
}

// Resolver : main resolver struct has an endpoint string to each service
type Resolver struct {
	Logger        *log.Logger
	Marnoto       string
	Dopamina      string
	Dora          string
	Sherlock      string
	Avenida       string
	Viveiro       string
	StorageBucket *services.StorageBucket
}

func headersFromContext(ctx context.Context) RequestHeaders {
	return RequestHeaders{
		authUser:  handlers.GetAuthUser(ctx),
		profileID: handlers.GetProfileID(ctx),
		userID:    handlers.GetUserID(ctx),
	}
}

// TODO - Hammer - dopamina needs always profileID header even if the user is anonymous
func addHammerProfileID(ctx context.Context) RequestHeaders {
	headers := headersFromContext(ctx)
	if headers.profileID == "" {
		headers.profileID = "-1"
	}
	return headers
}

// Challenge : challenge resolver
func (r *Resolver) Challenge() graphql.ChallengeResolver {
	return &challengeResolver{r}
}

// Post : post resolver
func (r *Resolver) Post() graphql.PostResolver {
	return &postResolver{r}
}

// PrivacyPrivateProfiles TODO - do we need this resolver?
// PrivacyPrivateProfiles : privacy private profiles resolver - get profiles information from a private challenge
func (r *Resolver) PrivacyPrivateProfiles() graphql.PrivacyPrivateProfilesResolver {
	return &privacyPrivateProfilesResolver{r}
}

// Comment : comment resolver
func (r *Resolver) Comment() graphql.CommentResolver {
	return &commentResolver{r}
}

// Clap : clap resolver
func (r *Resolver) Clap() graphql.ClapResolver {
	return &clapResolver{r}
}

// Group : group resolver
func (r *Resolver) Group() graphql.GroupResolver {
	return &groupResolver{r}
}

// Profile : profile resolver
func (r *Resolver) Profile() graphql.ProfileResolver {
	return &profileResolver{r}
}

// Mutation : mutation resolver
func (r *Resolver) Mutation() graphql.MutationResolver {
	return &mutationResolver{r}
}

// Query : query resolver
func (r *Resolver) Query() graphql.QueryResolver {
	return &queryResolver{r}
}

type challengeResolver struct{ *Resolver }

// TODO - maybe this resolver could be removed, we just want to see the information of creator (profile) of the challenge
func (r *challengeResolver) User(ctx context.Context, obj *models.Challenge) (*models.User, error) {
	return r.UserByID(ctx, obj.UserID)
}

type postResolver struct{ *Resolver }

// TODO - same here - in my opinion this resolver should be removed
func (r *postResolver) User(ctx context.Context, obj *models.Post) (*models.User, error) {
	return r.UserByID(ctx, obj.UserID)
}

type privacyPrivateProfilesResolver struct{ *Resolver }

// TODO - don't know if this resolver is being using or it's important in the future.
func (r *privacyPrivateProfilesResolver) Profiles(ctx context.Context, obj *models.PrivacyPrivateProfiles) ([]*models.Profile, error) {
	return profilesByIDs(ctx, r.Marnoto, r.Logger, obj.ProfileIDs)
}

type commentResolver struct{ *Resolver }

type clapResolver struct{ *Resolver }

// TODO - same here
func (r *clapResolver) Profile(ctx context.Context, obj *models.Clap) (*models.Profile, error) {
	return r.ProfileByID(ctx, obj.ProfileID)
}

type profileResolver struct{ *Resolver }

type groupResolver struct{ *Resolver }

type mutationResolver struct{ *Resolver }

// Helper functions for main Resolver

// UserByID : get user by id
func (r *Resolver) UserByID(ctx context.Context, id int) (*models.User, error) {
	r.Logger.Printf("Get user by ID: %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Marnoto, userURL, id)
	user := &models.User{}

	if err := getJSON(r.Logger, url, headersFromContext(ctx), user); err != nil {
		return nil, err
	}

	return user, nil
}

// ProfileByID : get profile by id
func (r *Resolver) ProfileByID(ctx context.Context, id int) (*models.Profile, error) {
	r.Logger.Printf("Get profile by ID: %d", id)

	url := fmt.Sprintf("%s%s/%d", r.Marnoto, profileURL, id)
	profile := &models.Profile{}

	if err := getJSON(r.Logger, url, headersFromContext(ctx), profile); err != nil {
		return nil, err
	}

	return profile, nil
}

type queryResolver struct{ *Resolver }

func (r *queryResolver) Profile(ctx context.Context, id int) (*models.Profile, error) {
	return r.ProfileByID(ctx, id)
}

func (r *queryResolver) ProfileByUsername(ctx context.Context, username string) (*models.Profile, error) {
	r.Logger.Printf("Get profile by username %s", username)

	url := fmt.Sprintf("%s%s?username=%s", r.Marnoto, profileURL, username)
	profile := &models.Profile{}

	if err := getJSON(r.Logger, url, headersFromContext(ctx), profile); err != nil {
		return nil, err
	}

	return profile, nil
}

func profilesByIDs(ctx context.Context, endpoint string, logger *log.Logger, ids []int) ([]*models.Profile, error) {
	arrayIDs := toStringArray(ids)
	queryParameters := strings.Join(arrayIDs, "&")

	url := fmt.Sprintf("%s%s?%s", endpoint, profilesURL, queryParameters)
	profiles := make([]*models.Profile, 0)

	if err := getJSON(logger, url, headersFromContext(ctx), &profiles); err != nil {
		return nil, err
	}

	return profiles, nil
}

func challengesByIDs(ctx context.Context, endpoint string, logger *log.Logger, ids []int) ([]*models.Challenge, error) {
	arrayIDs := toStringArray(ids)
	queryParameters := strings.Join(arrayIDs, "&")

	url := fmt.Sprintf("%s%s?%s", endpoint, challengesURL, queryParameters)

	var jsonObject []map[string]interface{}
	if err := getJSON(logger, url, addHammerProfileID(ctx), &jsonObject); err != nil {
		return nil, err
	}

	challenges := make([]*models.Challenge, 0)
	for _, data := range jsonObject {
		challenge, _ := jsonToChallenge(logger, data)
		challenges = append(challenges, challenge)
	}

	return challenges, nil
}

func (r *queryResolver) Search(ctx context.Context, text string) ([]*models.Profile, error) {
	r.Logger.Printf("Search profiles by text %s", text)

	url := fmt.Sprintf("%s%s?q=%s", r.Sherlock, searchURL, text)
	searchResult := &models.SearchResult{}

	if err := getJSON(r.Logger, url, headersFromContext(ctx), searchResult); err != nil {
		return nil, err
	}

	return profilesByIDs(ctx, r.Marnoto, r.Logger, searchResult.ProfileIDs)
}

func jsonToChallenge(logger *log.Logger, jsonObject map[string]interface{}) (*models.Challenge, error) {
	challenge := &models.Challenge{
		Privacy: handleChallengePrivacy(jsonObject["privacy"]),
		Media:   handleChallengeMedia(jsonObject["media"]),
	}

	data, _ := json.Marshal(jsonObject)
	if err := json.Unmarshal(data, challenge); err != nil {
		logger.Printf("error unmarshal json to challenge data %v with error %v", jsonObject, err)
		return nil, err
	}

	return challenge, nil
}

func handleChallengePrivacy(jsonObject interface{}) models.Privacy {
	privacyObject := jsonObject.(map[string]interface{})

	switch privacyObject["type"].(string) {
	case models.PrivacyTypePublic.String():
		return &models.PrivacyPublic{}
	case models.PrivacyTypePrivateProfiles.String():
		return &models.PrivacyPrivateProfiles{}
	case models.PrivacyTypePrivateGroups.String():
		return &models.PrivacyPrivateGroup{}
	default:
		return nil
	}
}

func handleChallengeMedia(jsonObject interface{}) models.ChallengeMedia {
	mediaObject := jsonObject.(map[string]interface{})

	switch mediaObject["type"].(string) {
	case models.ChallengeMediaTypeImage.String():
		return &models.ChallengeMediaImage{}
	case models.ChallengeMediaTypeVideo.String():
		return &models.ChallengeMediaVideo{}
	default:
		return nil
	}
}

func (r *queryResolver) FeaturedProfiles(ctx context.Context) ([]*models.Profile, error) {
	r.Logger.Print("Get featured profiles")

	url := fmt.Sprintf("%s%s%s", r.Dora, featuredURL, profilesURL)
	ids := make([]int, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &ids); err != nil {
		return nil, err
	}

	return profilesByIDs(ctx, r.Marnoto, r.Logger, ids)
}

func (r *queryResolver) FeaturedChallenges(ctx context.Context) ([]*models.Challenge, error) {
	r.Logger.Print("Get featured challenges")

	url := fmt.Sprintf("%s%s%s", r.Dora, featuredURL, challengesURL)
	ids := make([]int, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &ids); err != nil {
		return nil, err
	}

	return challengesByIDs(ctx, r.Dopamina, r.Logger, ids)
}

func (r *queryResolver) Trending(ctx context.Context) ([]*models.Challenge, error) {
	r.Logger.Print("Get trending challenges")

	url := fmt.Sprintf("%s%s%s", r.Dora, trendingURL, challengesURL)
	ids := make([]int, 0)

	if err := getJSON(r.Logger, url, headersFromContext(ctx), &ids); err != nil {
		return nil, err
	}

	return challengesByIDs(ctx, r.Dopamina, r.Logger, ids)
}

func jsonToPost(logger *log.Logger, jsonObject map[string]interface{}) (*models.Post, error) {
	post := &models.Post{
		Media: handlePostMedia(jsonObject),
	}

	data, _ := json.Marshal(jsonObject)
	if err := json.Unmarshal(data, post); err != nil {
		logger.Printf("error unmarshal json to post with error %v", err)
		return nil, err
	}

	return post, nil
}

func handlePostMedia(jsonObject interface{}) models.PostMedia {
	mediaObject := jsonObject.(map[string]interface{})

	switch mediaObject["type"].(string) {
	case models.PostTypeImage.String():
		return &models.PostMediaImage{}
	case models.PostTypeVideo.String():
		return &models.PostMediaVideo{}
	case models.PostTypeText.String():
		return &models.PostMediaText{}
	default:
		return nil
	}
}

func (r *queryResolver) Feed(ctx context.Context, limit int, nextToken *string, anonymous bool) (*models.FeedResult, error) {
	r.Logger.Printf("Get feed with limit %d - is anonymous? %t", limit, anonymous)

	requestHeaders := headersFromContext(ctx)

	// Anonymous feed request doesn't have ProfileID and UserID headers, so we need to exclude these headers on this request
	if anonymous {
		requestHeaders.profileID = ""
		requestHeaders.userID = ""
	}

	url := fmt.Sprintf("%s%s?limit=%d", r.Avenida, feedURL, limit)

	if nextToken != nil {
		url = fmt.Sprintf("%s&nextToken=%s", url, *nextToken)
	}

	items := &models.FeedItems{}
	if err := getJSON(r.Logger, url, requestHeaders, items); err != nil {
		return nil, err
	}

	requestBody, _ := json.Marshal(items.Items)
	url = fmt.Sprintf("%s/dopamina%s", r.Dopamina, feedURL)

	var jsonObject []map[string]interface{}
	if err := postJSON(r.Logger, url, requestHeaders, requestBody, &jsonObject); err != nil {
		r.Logger.Printf("Failed post json with error %v", err)
		return nil, err
	}

	feedResult := &models.FeedResult{
		Items:     jsonToFeed(r.Logger, jsonObject),
		NextToken: items.NextToken,
	}

	return feedResult, nil
}

func jsonToFeed(logger *log.Logger, jsonObject []map[string]interface{}) []models.Feed {
	feed := make([]models.Feed, 0)

	for _, data := range jsonObject {

		switch data["type"].(string) {
		case models.FeedTypeChallenge.String():
			challenge, _ := jsonToChallenge(logger, data["item"].(map[string]interface{}))
			feed = append(feed, challenge)
		case models.FeedTypePost.String():
			post, _ := jsonToPost(logger, data["item"].(map[string]interface{}))
			feed = append(feed, post)
		}
	}

	return feed
}

func (r *queryResolver) HasParticipated(ctx context.Context, challengeID int) (bool, error) {
	headers := headersFromContext(ctx)

	r.Logger.Printf("Check if current profile %s participate on challengeID %d", headers.profileID, challengeID)

	url := fmt.Sprintf("%s%s?challengeID=%d", r.Dopamina, postURL, challengeID)
	_, err := validateRequest(r.Logger, http.MethodGet, nil, url, headers)
	if err != nil {
		return false, nil
	}

	return true, nil
}

func toStringArray(array []int) []string {
	var result []string
	for _, value := range array {
		result = append(result, "id[]="+strconv.Itoa(value))
	}

	return result
}
