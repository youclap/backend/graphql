FROM golang:1.12-alpine

COPY bin/graphql .
COPY firebase-admin.json .

COPY dev.yml .
COPY prod.yml .

ENV PORT=80
ENV ENVIRONMENT=prod

EXPOSE ${PORT}

ENTRYPOINT ./graphql --port ${PORT}
