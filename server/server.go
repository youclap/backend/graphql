package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"cloud.google.com/go/storage"
	"github.com/99designs/gqlgen/handler"
	"github.com/go-chi/chi"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"gitlab.com/youclap/backend/graphql"
	"gitlab.com/youclap/backend/graphql/handlers"
	"gitlab.com/youclap/backend/graphql/resolvers"
	"gitlab.com/youclap/backend/graphql/services"
	"google.golang.org/api/option"
)

const defaultPort = "8080"
const defaultEnv = "local"

const MEGABYTE int64 = (1 << 20)

var (
	storageClient *storage.Client
	mainContext   context.Context
)

var logger *log.Logger

func main() {

	router := chi.NewRouter()
	router.Use(handlers.Middleware())
	router.Use(cors.AllowAll().Handler)

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	env := os.Getenv("ENVIRONMENT")
	if env == "" {
		env = defaultEnv
	}

	godotenv.Load(env + ".yml")

	logger = log.New(os.Stdout, "graphQL-server: ", log.LstdFlags)
	logger.Printf("environment %s", env)

	// --- Upload file ---
	// TODO - change this upload limits memory and size
	uploadMaxMemory := handler.UploadMaxMemory(512 * MEGABYTE)
	uploadMaxSize := handler.UploadMaxSize(1024 * MEGABYTE)

	mainContext = context.Background()

	if err := setupStorageClient(mainContext); err != nil {
		logger.Fatalf("setup storage client failed with error %v", err)
	}

	bucketName := os.Getenv("BUCKET_NAME")
	bucket, err := setupStorageBucket(mainContext, bucketName, logger)
	if err != nil {
		logger.Fatalf("setup storage bucket name %s with error %v", bucketName, err)
	}

	resolver := &resolvers.Resolver{
		Logger:        logger,
		Marnoto:       os.Getenv("ENDPOINT_MARNOTO"),
		Dopamina:      os.Getenv("ENDPOINT_DOPAMINA"),
		Dora:          os.Getenv("ENDPOINT_DORA"),
		Sherlock:      os.Getenv("ENDPOINT_SHERLOCK"),
		Avenida:       os.Getenv("ENDPOINT_AVENIDA"),
		Viveiro:       os.Getenv("ENDPOINT_VIVEIRO"),
		StorageBucket: bucket,
	}

	router.Handle("/playground", handler.Playground("GraphQL playground", "/playground"))
	router.Handle("/healthcheck", healthcheck())
	router.Handle("/", handler.GraphQL(graphql.NewExecutableSchema(graphql.Config{Resolvers: resolver}), uploadMaxMemory, uploadMaxSize))

	logger.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	if err := http.ListenAndServe(":"+port, router); err != nil {
		panic(err)
	}
}

func healthcheck() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
	})
}

func setupStorageClient(ctx context.Context) error {
	credsFilePath := os.Getenv("FIREBASE_SERVICE_ACCOUNT")
	if credsFilePath != "" {
		client, err := storage.NewClient(ctx, option.WithCredentialsFile(credsFilePath))
		storageClient = client

		return err
	}

	// For Local environment
	client, err := storage.NewClient(ctx)
	storageClient = client

	return err
}

func setupStorageBucket(ctx context.Context, bucketName string, logger *log.Logger) (*services.StorageBucket, error) {
	return &services.StorageBucket{
		Client:     storageClient,
		Bucket:     storageClient.Bucket(bucketName),
		BucketName: bucketName,
		Ctx:        ctx,
		Logger:     logger,
	}, nil
}
