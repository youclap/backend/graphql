# graphQL

Run for the pipelines:
- `go run github.com/99designs/gqlgen`

This will generate `generated.go` (graphql stuff) and `models_gen.go` (for your models) files. It's necessary for the resolvers to work.

Run the server:
- Run graphql server: `go run server/server.go`
